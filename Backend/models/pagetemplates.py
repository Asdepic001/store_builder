

import os
from typing import Literal
from models.base import BaseModel, Base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, Float, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column
import models

class Pagetemplates(BaseModel, Base):
    
    
    __tablename__ = 'pagetemplates'
    name_page : Mapped[str] = mapped_column(String(255))
    template_id : Mapped[str] = mapped_column(ForeignKey("templates.id"))
    template = relationship("Templates")
    
    def __init__(self, name_page, template_id, props = None):
        super().__init__(props)
        self.name_page = name_page
        self.template_id = template_id