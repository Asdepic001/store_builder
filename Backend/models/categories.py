

import os
from typing import Literal
from models.base import BaseModel, Base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, Float, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column
import models

class Categories(BaseModel, Base):
    
    
    __tablename__ = 'categories'
    category_name : Mapped[str] = mapped_column(String(255))
    
    def __init__(self, category_name, props = None):
        super().__init__(props)
        self.category_name = category_name