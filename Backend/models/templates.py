

import os
from typing import Literal
from models.base import BaseModel, Base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, Float, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column
import models

class Templates(BaseModel, Base):
    
    
    __tablename__ = 'templates'
    uuid : Mapped[str] = mapped_column(String(255))
    template_name : Mapped[str] = mapped_column(String(255))
    primary_color : Mapped[str] = mapped_column(String(255), nullable=True)
    secondary_color : Mapped[str] = mapped_column(String(255), nullable=True)
    tertiary_color : Mapped[str] = mapped_column(String(255), nullable=True)
    
    def __init__(self, uuid, template_name, primary_color, secondary_color, tertiary_color, props = None):
        super().__init__(props)
        self.uuid = uuid
        self.template_name = template_name
        self.primary_color = primary_color
        self.secondary_color = secondary_color
        self.secondary_color = secondary_color
        self.tertiary_color = tertiary_color