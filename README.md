# Une fois le projet cloné :

## Intallation et activation de venv
Premièrement être dans le dossier Backend.
Commandes :
- pip install virtualenv
- virtualenv venv
Pour activer votre venv, il faut taper la commande :
- .\venv\Scripts\activate

## Installation des dépendances
Pour installer les dépendances vous avez deux possibilités :
Soit vous install chaque dépendance dont vous aurez besoin
Exemple :
- pip install fastapi
- pip install sqlalchemy
- pip install uvicorm
Ou soit vous taper cette commande : pip install -r requirements.txt. Cette commande installer directement toutes les dépendances se trouvant dans votre fichier requirements.txt.

## Démarrer le projet
Commande : uvicorn run:app --reload

NB: Tant que vous n'activez pas votre venv, vous ne pouvez pas démarrer le projet. 